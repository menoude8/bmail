//                                             
// RUSH EPITECH
// Made by menoude hountondji                                                             
// Login   <hounto_m@epitech.net>                                                  
//

#include "my_serv.h"

int	count_pop(void)
{
  struct dirent *lecture;
  DIR		*rep;
  char		userr[1024] = "";
  char		*user = cherche_file("POP_USER");
  int		r = 0;

  strcat(userr, "MAIL/");
  strcat(userr,user);
  mkdir(userr,777);
  strcat(userr,"/pop");
  mkdir(userr,777);
  rep = opendir(userr);
  while ((lecture = readdir(rep)))
      r++;
  r = r - 2;
  closedir(rep);  
  return r;
}

int	count_imap(void)
{
  struct dirent *lecture;
  DIR		*rep;
  char		userr[1024] = "";
  char		*user = cherche_file("IMAP_USER");
  int		r = 0;

  strcat(userr, "MAIL/");
  strcat(userr,user);
  mkdir(userr,777);
  strcat(userr,"/imap");
  mkdir(userr,777);
  rep = opendir(userr);
  while ((lecture = readdir(rep)))
    r++;
  r = r - 2;
  closedir(rep);
  return r;
}
