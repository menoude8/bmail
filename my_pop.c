//                                                                          
// RUSH EPITECH                                                             
// Made by menoude hountondji                                                
// Login   <hounto_m@epitech.net>                            
//   

#include "my_serv.h"

int NB_MAIL = 0;

int	nb_linge(char **str)
{
  int i = 0;
 
  while (str[i] != NULL)
    i++;
  return i;
}

int	my_pop_nbr(char *str)
{
  char **res;
  char temp[1024] = "";
  
  strcat(temp,str);
  res = str_split(temp, " ");
  return atoi(res[0]);
}

void	gener_uidl_fils(char *str)
{
  FILE *fichier = NULL;
  fichier = fopen("./uidl.txt", "a+");
  if (fichier)
    {
      fputs(str,fichier);
      fputs("\n",fichier);
    }
  fclose(fichier);
}


int	verif_uidl(char *str)
{
  FILE *fichier = NULL;
  char chaine[1024] = "";
  char man[128] = "";
  char **res;
  
  fichier = fopen("./uidl.txt", "a+");
  res = str_split(str, " ");
  if (fichier != NULL)
    {
      while (fgets(chaine, 1024, fichier) != NULL)
	{
	  if (strncmp(chaine, res[1], my_strlen(res[1])) == 0)
	    return 0;
	}
    }
  gener_uidl_fils(res[1]);
  return 1;
}

int	view_back(char *str)
{
  int i = 0;
  while (str[i])
    {
      if (str[i] == '\0')
	  return 1;
      i++;
    }
  return 0;
}

int	readn(int fd, char *ptr, int n)
{
  int nl = 0;
  int nr = 0;
  int retry = 0;
  
  nl = n;
  bzero(ptr,n);
  while ( nl > 0 ) 
    {
      nr = read(fd,ptr,nl);
      if (nr < 0 ) {
	if (errno != EAGAIN) { printf("EAGAIN\n"); exit(1); }
	retry++;
	usleep(100);
	if ( retry >= 5 ) return nr;     /*error*/
      } else {
	if ( nr == 0 )
	  break;
      }
      nl -= nr;
      ptr += nr;
      if ( *(ptr-2) == '\r' && *(ptr-1) == '\n' )
	break;
    }
  *ptr = 0x00;
  return (n-nl);
}

void	cherche_uidl(char *str, SOCKET sock, char *userr)
{
  char **res;
  int i = 0;
  int nb = 0;
  int r;
  char path[128] = "";
  char recv[42000] = "";
  char send[1024] = "";
  FILE *fichier =  NULL; 
  char *timy;
  
  res = str_split(str, "\n");
  printf("+OK start dl\n");
  while (res[i]) 
    {
      printf("+OK test mail\n");
      if (i > 0 && res[i][0] != '.')
	{
	  if (verif_uidl(res[i]) == 1)
	    {
	      printf("+OK -NO EXIST-\n");
	      sprintf(path,"%s/%i.mail",userr,NB_MAIL);
	      printf("<<%s>>\n",path);
	      NB_MAIL++;
	      nb = my_pop_nbr(res[i]);
	      fichier = fopen(path, "a+");	      
	      sprintf(send, "RETR %i\r\n",nb);
	      write(sock,send,my_strlen(send));
	      r = readn(sock, recv, 42000);
	      printf("+ OK - MAIL RECV !\n");
	      fputs(recv,fichier);
	      fclose(fichier);
	    }
	  else
	    printf("+OK -EXIST-\n");
	}
      i++;
    }
}

int	pop_maj(void)
{
  char send[1024] = "";
  char recv[1024] = "";
  char *user = cherche_file("POP_USER");
  char *pass = cherche_file("POP_PASS");
  int port = atoi(cherche_file("POP_PORT"));
  char *address = cherche_file("POP_SERV");
  SOCKET sock = init_connection(address, port);
  char userr[254] = "";
  
  strcat(userr, "./MAIL/");
  strcat(userr,user);
  mkdir(userr, 777);
  strcat(userr,"/pop");
  mkdir(userr, 777);
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv); 
  sprintf(send, "USER %s\r\n",user);
  write(sock,send,my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);
  sprintf(send, "PASS %s\r\n",pass);
  write(sock,send, my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);
  if (recv[0] != '+')
    {
      printf("-ERROR login or password !\n");
      write(sock,"QUIT\r\n", 6);
      return;
    }
  sprintf(send, "UIDL\r\n");
  write(sock,send, my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    {
      cherche_uidl(recv, sock, userr);
      printf("+OK QUIT\n");
      write(sock,"QUIT\r\n", 6);
      end_connection(sock);
      end();
    }
  return 1;
}
