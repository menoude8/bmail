//
// RUSH EPITECH
// Made by menoude hountondji
// Login   <hounto_m@epitech.net>
// 

#include "my_serv.h"

int NBR_MAIL_NMAP = 0;

int	auth_imap(SOCKET sock, char *user, char *pass)
{
  char	send[1024] = "";
  char	recv[1024] = "";
  sprintf(send ,"001 login %s %s\n" ,user ,pass);
  write(sock, send, my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);
  if (recv[4] == 'O')
      return 1;
  else
    return 0;
}

void	init_imap(SOCKET sock)
{
  char          send[1024] = "";
  char          recv[1024] = "";

  sprintf(send ,"a2 select inbox\n");
  write(sock, send, my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);
}

void	logout_imap(SOCKET sock)
{
  char          send[1024] = "";
  char          recv[1024] = "";

  sprintf(send ,"a002 logout\n");
  write(sock, send, my_strlen(send));
  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);
}

int	my_imap(void)
{
  char		send[1024] = "";
  char		recv[42000] = "";
  char		*user = cherche_file("IMAP_USER");
  char		*pass = cherche_file("IMAP_PASS");
  int		port = atoi(cherche_file("IMAP_PORT"));
  char		*address = cherche_file("IMAP_SERV");
  SOCKET	sock = init_connection(address, port);
  char userr[254] = ""; 
  char		path[1024] = "";
  FILE *fichier =  NULL;
  int i = 0;
  int t = 1;
  int r = 0;

  if (read(sock, &recv, 1024) > 1)
    printf("%s",recv);

  if (auth_imap(sock, user, pass) == 1)
    {
      strcat(userr, "./MAIL/");
      strcat(userr,user);
      mkdir(userr, 777);
      strcat(userr,"/imap");
      mkdir(userr, 777);
      init_imap(sock);     
      while (i == 0)
	{
	  sprintf(send ,"a6 fetch %i body[header]\n",t);
          write(sock, send, my_strlen(send));
          r = readn(sock, recv, 42000);
	  printf("<<%s>>\n",recv);
	  if (my_strlen(recv) > 42)
	    {
	      NBR_MAIL_NMAP++;
	      sprintf(path, "%s/%i.mail",userr, NBR_MAIL_NMAP);
	      fichier = fopen(path,"a+");
	      fputs(recv, fichier);
	      fclose(fichier);
	    }
	  else
	    i = 1;
	  t++;


	}
     logout_imap(sock);
    }
  else
    printf("ERROR login or password\n");
  end_connection(sock);
  end();
  return 1;
}
