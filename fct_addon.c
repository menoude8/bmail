#include "main.h"

static GtkWidget *text_view;

void	on_save_button(GtkWidget *widget, GtkWidget *W)
{
  GtkWidget *dialog;
  GtkTextBuffer* text_buffer=0;
  GtkTextIter start;
  GtkTextIter end;
  gchar* buf=0;
  FILE *fichier;

  remove("./CONFIG.txt");
  text_buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
  gtk_text_buffer_get_start_iter(text_buffer,&start);
  gtk_text_buffer_get_end_iter(text_buffer,&end);
  buf = gtk_text_buffer_get_text(text_buffer,&start, &end,TRUE);
  fichier = fopen("./CONFIG.txt","a+");
  if(fichier == NULL)
    {
      GtkWidget *dialog;

      dialog = gtk_message_dialog_new(GTK_WINDOW(text_view), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Impossible d'ouvrir le fichier \n");
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
      return;
    }
  fputs((char *)buf,fichier);
  fclose(fichier);
  gtk_widget_destroy(W);
}

void b4(GtkWidget *widget, gpointer data)
{
  GtkWidget* window;
  GtkWidget* box;
  GtkWidget* button;
  GtkWidget* pButton;
  GtkWidget *scrollbar;
  GtkTextBuffer *buffer;
  GtkTextIter start;
  GtkTextIter end;
  FILE *fichier;
  const gchar *chemin;
  gchar lecture[1084];

  
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 500);
  gtk_window_set_title(GTK_WINDOW(window), "Conf");
  g_signal_connect(G_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),0);
  box=gtk_vbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),box);
  scrollbar = gtk_scrolled_window_new(NULL, NULL);
  gtk_box_pack_start(GTK_BOX(box), scrollbar, TRUE, TRUE, 5);
  text_view=gtk_text_view_new();
  gtk_container_add(GTK_CONTAINER(scrollbar),text_view);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  pButton = gtk_button_new_with_label("Save");
  gtk_box_pack_start(GTK_BOX(box), pButton, FALSE, TRUE, 0);
  g_signal_connect(G_OBJECT(pButton), "clicked", G_CALLBACK(on_save_button), window);
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
  fichier = fopen("./CONFIG.txt","a+");
  if(fichier == NULL)
    {
      GtkWidget *dialog;

      dialog = gtk_message_dialog_new(GTK_WINDOW(text_view), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Impossible d'ouvrir le fichier : \n%s", g_locale_to_utf8(chemin, -1, NULL, NULL, NULL));
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
      gtk_widget_destroy(window);
      return;
    }
  gtk_text_buffer_get_start_iter(buffer,&start);
  gtk_text_buffer_get_end_iter(buffer,&end);
  gtk_text_buffer_delete(buffer, &start, &end);
  while(fgets(lecture, 1024, fichier))
    {
      gtk_text_buffer_get_end_iter(buffer,&end);
      gtk_text_buffer_insert(buffer, &end, g_locale_to_utf8(lecture, -1, NULL, NULL, NULL), -1);
    }
  fclose(fichier);
  gtk_widget_show_all(window);
}


void b3(GtkWidget *widget, gpointer data)

{  
  g_print("TEST\n");
    mkdir("./MAIL", 777);
    g_print("TEST\n");
    my_imap();
    g_print("TEST\n");
}

void b2(GtkWidget *widget, gpointer data)
{
  t_gtk3	g;

  mkdir("./MAIL", 777);
  pop_maj();
}

void b5(GtkWidget *widget, gpointer data)
{
  exit(1);
}


