NAME	=	bmail

SRC	=	main.c		\
		fct.c		\
		fct_addon.c	\
		my_fonctions.c  \
		my_pop.c	\
		my_smtp.c	\
		init_sock.c	\
		my_b64.c	\
		my_imap.c

CC	=	cc -o

GTK	=	`pkg-config --cflags --libs gtk+-2.0`

INCLUDE	=	-I/usr/include/gtk-2.0/

all	:
		$(CC) $(NAME) $(SRC) $(GTK) $(INCLUDE)

clean	:
		rm -f *~
		rm -f *.o

fclean	:	clean
		rm -f $(NAME)

re	:	fclean all