#include "main.h"

void suite2(t_gtk2 *s)
{
    s->TexteConverti = g_locale_to_utf8("<span face=\"Verdana\" foreground=\"#73b5ff\" size=\"medium\"><b>Destinataire</b></span>", -1, NULL, NULL, NULL);
  s->Label=gtk_label_new(s->TexteConverti);
  g_free(s->TexteConverti);
  gtk_label_set_use_markup(GTK_LABEL(s->Label), TRUE);
  gtk_label_set_justify(GTK_LABEL(s->Label), GTK_JUSTIFY_CENTER);
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->Label, FALSE, TRUE, 0);
  s->pEntry2 = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->pEntry2, FALSE, TRUE, 0);
  s->TexteConverti2 = g_locale_to_utf8("<span face=\"Verdana\" foreground=\"#73b5ff\" size=\"medium\"><b>Objet</b></span>", -1, NULL, NULL, NULL);
  s->Label2=gtk_label_new(s->TexteConverti2);
  g_free(s->TexteConverti2);
  gtk_label_set_use_markup(GTK_LABEL(s->Label2), TRUE);
  gtk_label_set_justify(GTK_LABEL(s->Label2), GTK_JUSTIFY_CENTER);
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->Label2, FALSE, TRUE, 0);
  s->pEntry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->pEntry, FALSE, TRUE, 0);
  s->text_view=gtk_text_view_new();
  gtk_box_pack_start(GTK_BOX(s->pVBox),s->text_view,TRUE,FALSE,1);
  s->pButtonbb = gtk_button_new_with_label("Bmail Bomber");
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->pButtonbb, FALSE, TRUE, 0);
  s->pButton = gtk_button_new_with_label("Envoyer");
  gtk_box_pack_start(GTK_BOX(s->pVBox), s->pButton, FALSE, TRUE, 0);
}

void on_activate_entry(GtkWidget *pEntry, gpointer data)
{
  const gchar *sText;

  isobject = 1;
  sText = gtk_entry_get_text(GTK_ENTRY(pEntry));
  object = g_malloc(512 * sizeof(gchar));
  g_sprintf(object, "%s", sText);  
}

void on_activate_entry2(GtkWidget *pEntry, gpointer data)
{
  const gchar *sText;

  isadress = 1;
  sText = gtk_entry_get_text(GTK_ENTRY(pEntry));
  adress = g_malloc(512 * sizeof(gchar));
  g_sprintf(adress, "%s", sText);
}

void on_send_button(GtkButton *button, t_gtk2 *s)
{
  GtkWidget *dialog;
  GtkTextBuffer* text_buffer=0;
  GtkTextIter start;
  GtkTextIter end;
  gchar* buf=0;

  text_buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(s->text_view));
  gtk_text_buffer_get_start_iter(text_buffer,&start);
  gtk_text_buffer_get_end_iter(text_buffer,&end);
  buf=gtk_text_buffer_get_text(text_buffer,&start, &end,TRUE);
  printf("[%s]\n", (char *)adress);
  my_smtp((char *)buf, (char *)adress, (char *)object);
  g_free(buf);
  gtk_widget_destroy(s->pWindow);
}

void on_send_buttonbb(GtkButton *button, t_gtk2 *s)
{
  GtkWidget *dialog;
  GtkTextBuffer* text_buffer=0;
  GtkTextIter start;
  GtkTextIter end;
  gchar* buf=0;
  int		i = 0;

  text_buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(s->text_view));
  gtk_text_buffer_get_start_iter(text_buffer,&start);
  gtk_text_buffer_get_end_iter(text_buffer,&end);
  buf = gtk_text_buffer_get_text(text_buffer,&start, &end,TRUE);
  printf("[%s]\n", (char *)adress);
  while (i < 20)
    {
      my_smtp((char *)buf, (char *)adress, (char *)object);
      usleep(200);
      i++;
    }
  g_free(buf);
  gtk_widget_destroy(s->pWindow);
}

void init2(t_gtk2 *s)
{
  
  s->TexteConverti = NULL;
  s->TexteConverti2 = NULL;
  s->pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(s->pWindow), "Envoie Bmail");
  gtk_window_set_default_size(GTK_WINDOW(s->pWindow), 800, 600);
  g_signal_connect(G_OBJECT(s->pWindow), "destroy", G_CALLBACK(gtk_main_quit), NULL);
  s->pVBox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(s->pWindow), s->pVBox);
  suite2(s);
  g_signal_connect(G_OBJECT(s->pEntry), "activate", G_CALLBACK(on_activate_entry2), NULL);
  g_signal_connect(G_OBJECT(s->pEntry2), "activate", G_CALLBACK(on_activate_entry), NULL);
  g_signal_connect(G_OBJECT(s->pButton), "clicked", G_CALLBACK(on_send_button), s);
  g_signal_connect(G_OBJECT(s->pButtonbb), "clicked", G_CALLBACK(on_send_buttonbb), s);
  gtk_widget_show_all(s->pWindow);
  gtk_main();
}

void b1(GtkWidget *widget, gpointer data)
{
  t_gtk2		gtk2;
  
  init2(&gtk2);
}


